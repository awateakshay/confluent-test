from confluent_kafka import Producer

kafka_user = 'something'
kafka_password = 'something'
some_data_source = ['a','b','c','d']
p = Producer({'bootstrap.servers': ['10.96.xx.xx:19092, 10.96.xx.xx:19092, 10.96.xx.xx:19092'], 'sasl.username': kafka_user, 'sasl.password': kafka_password,
'security.protocol': 'SASL_PLAINTEXT',
'sasl.mechanisms':'PLAIN'
})

def delivery_report(err, msg):
    """ Called once for each message produced to indicate delivery result.
        Triggered by poll() or flush(). """
    if err is not None:
        print('Message delivery failed: {}'.format(err))
    else:
        print('Message delivered to {} [{}]'.format(msg.topic(), msg.partition()))

for data in some_data_source:
    p.poll(0)
    p.produce('mytopic', data.encode('utf-8'), callback=delivery_report)
p.flush()
