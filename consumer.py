from confluent_kafka import Consumer
kafka_user = 'something'
kafka_password = 'something'


c = Consumer({
    'bootstrap.servers': ['10.96.xx.xx:19092, 10.96.xx.xx:19092, 10.96.xx.xx:19092'], 'sasl.username': kafka_user, 'sasl.password': kafka_password,
    'security.protocol': 'SASL_PLAINTEXT',
    'sasl.mechanisms':'PLAIN',
    'group.id': 'mygrou',
    'auto.offset.reset': 'earliest'
})

c.subscribe(['mytopic'])

while True:
    msg = c.poll(1.0)

    if msg is None:
        continue
    if msg.error():
        print("Consumer error: {}".format(msg.error()))
        continue

    print('Received message: {}'.format(msg.value().decode('utf-8')))

c.close()
